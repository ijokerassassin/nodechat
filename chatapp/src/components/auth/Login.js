import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import classnames from "classnames";


class Login extends Component {
	constructor() {
		super();

		this.state = {
			email: "",
			password: "",
			errors: {}
		};
	};

	componentDidMount() {
		// If logged in and user navigates to Login page, should redirect them to dashboard
		if (this.props.auth.isAuthenticated) {
		  this.props.history.push("/dashboard");
		}
	};

	onChange = e => {
		this.setState({ [e.target.id] : e.target.value });
	};

	onSubmit = e => {
		e.preventDefault();

		const userData = {
			email: this.state.email,
			password: this.state.password
		};

		console.log(userData);
		this.props.loginUser(userData);

		if (this.props.auth.isAuthenticated) {
			this.props.history.push("/dashboard");
		  }
	};

	render() {
		const { errors } = this.state;

		return (
						<form noValidate onSubmit={this.onSubmit}>
							<p className="h4 text-center mb-4">Sign up</p>

							<div>
								<label htmlFor="email">Email</label>
								<input
									onChange={this.onChange}
									value={this.state.email}
									error={errors.email}
									id="email"
									type="email"
									className={classnames("", {
										invalid: errors.email || errors.emailnotfound
									})}
								/>
								<span className="red-text">
									{errors.email}
									{errors.emailnotfound}
								</span>
							</div>

							<div>
								<label htmlFor="password">Password</label>
								<input
									onChange={this.onChange}
									value={this.state.password}
									error={errors.password}
									id="password"
									type="password"
									className={classnames("", {
										invalid: errors.password || errors.passwordincorrect
									})}
								/>
								<span className="red-text">
									{errors.password}
									{errors.passwordincorrect}
								</span>
							</div>

							<div className="text-center mt-4">
					
								<button type="submit">
								Log in
								</button>
								<p>
									Heb je nog geen account? <Link to="/register">Registeer</Link>
								</p>
							</div>
						</form>
		
		);
	}
}

Login.propTypes = {
	loginUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
	errors: state.errors
});

// export default Login;
export default connect(
	mapStateToProps,
	{ loginUser }
)(Login);