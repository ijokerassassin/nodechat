import React from "react";
import { Link } from "react-router-dom";


const Landing = () => {
  return (
    <Link
      to="/login"
      style={{
        width: "140px",
        borderRadius: "3px",
        letterSpacing: "1.5px"
      }}
    >
      Log In
    </Link>
  );
}

export default Landing;