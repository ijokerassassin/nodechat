import React, { Component } from "react";
import { Link } from "react-router-dom";

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = theme => ({
	root: {
	  flexGrow: 1,
	},
	menuButton: {
	  marginRight: theme.spacing(2),
	},
	title: {
	  flexGrow: 1,
	  textDecoration: 'none',
	  color: 'white'
	},
});

class Navbar extends Component {
	state = {
		isOpen: false
	};
	  
	toggleCollapse = () => {
		this.setState({ isOpen: !this.state.isOpen });
	}

	render() {
		const { classes } = this.props;

		return (
			<AppBar position="static" color="secondary">
				<Toolbar>
					<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" className={classes.title} component={Link} to={"/"}>
						WhatsEpp
					</Typography>
					<Button color="inherit" component={Link} to={"/login"} >
						login
					</Button>
				</Toolbar>
			</AppBar>
		);
	}
};

export default withStyles(useStyles)(Navbar);