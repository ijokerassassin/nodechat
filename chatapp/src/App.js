import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import { Provider } from "react-redux";
import store from "./store";

import { Container, ThemeProvider } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';

import theme from './components/themes/defaultTheme';

import Navbar from "./components/layout/Navbar";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import PrivateRoute from "./components/private-route/PrivateRoute";
import Dashboard from "./components/dashboard/Dashboard";

//check for token to keep user logged in
if(localStorage.jwtToken) {
  //set auth token to header
  const token = localStorage.jwtToken;
  setAuthToken(token);
  //decode token and get user info 
  const decoded = jwt_decode(token);

  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  //check for expired token
  const currentTime = Date.now() / 1000; //to get in miliseconds
  if(decoded.exp < currentTime) {
    //logout user 
    store.dispatch(logoutUser());

    //redirect to loginpage
    window.location.href = "./login";
  }
}

const useStyles = makeStyles((theme) => ({
  app: {
    top: '20px',
    position: 'relative',
    backgroundColor: '#3B1F2B'
  }
}));

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Provider store={store}>
        <Router>
          <div className={classes.app}>
            <Container fixed> 
              <Navbar/>
              <Route exact path="/" component={Landing} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Switch>
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
              </Switch>
            </Container>
          </div>
        </Router>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
