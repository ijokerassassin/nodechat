import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import {
	GET_ERRORS,
	SET_CURRENT_USER,
	USER_LOADING
} from "./types";

//Post userdata to register user
export const registerUser = (userData, history) => dispatch => {
	axios
		.post("/api/users/register", userData)
		.then(res => history.push("/login"))
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		}
	);
};

//post userdata to log user in
export const loginUser = userData =>  dispatch => {
	axios
		.post("/api/users/login", userData)
		.then(res => {
			//save to localstorage

			//set token to localstorage
			const { token } = res.data;
			localStorage.setItem("jwtToken", token);

			//set token to authheader
			setAuthToken(token);

			//decode token to get userdata
			const decoded = jwt_decode(token);
			//set current user
			dispatch(setCurrentUser(decoded));
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		}
	
		);
};

export const setCurrentUser = decoded => {
	return {
		type: SET_CURRENT_USER,
		payload: decoded
	};
};

//set user loading state
export const setUserLoading = () => {
	return {
		type: USER_LOADING
	};
};

//log user out
export const logoutUser = () => dispatch => {
	//remove token from localstorage
	localStorage.removeItem("jwtToken");

	//remove auth header from future requests
	setAuthToken(false);

	// set current user to empty objectt {} witch will set isAuthenticated to false
	dispatch(setCurrentUser({}));
};