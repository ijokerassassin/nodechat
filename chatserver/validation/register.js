const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateRegisterInput(data) {
	let errors = {};

	console.log("REGISTER.JS")
	console.log(data);
	data.name = !isEmpty(data.name) ? data.name : "";
	data.email = !isEmpty(data.email) ? data.email : "";
	data.password = !isEmpty(data.password) ? data.password : "";
	data.password2 = !isEmpty(data.password2) ? data.password2 : "";

	//Check name
	if(Validator.isEmpty(data.name)) {
		errors.name = "Naam is verplicht";
	}

	//Check email
	if(Validator.isEmpty(data.email)) {
		errors.email = "Email is verplicht";
	} else if (!Validator.isEmail(data.email)) {
		errors.email = "Email is ongeldig";
	}

	if(Validator.isEmpty(data.password)) {
		errors.password = "Wachtwoord is verplicht";
	}

	if(!Validator.isLength(data.password, {mix: 6, max: 30})) {
		errors.password = "Wachtwoord moet minimaal 6 characters zijn";
	}

	if(!Validator.equals(data.password, data.password2)) {
		errors.password2 = "Wachtwoorden komen niet overeen";
	}

	return {
		errors,
		isValid: isEmpty(errors)
	};
};