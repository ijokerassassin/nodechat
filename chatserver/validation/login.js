const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateLoginInput(data) {
	let errors = {};

	data.email = !isEmpty(data.email) ? data.email : "";
	data.password = !isEmpty(data.password) ? data.password : "";

	//Check email
	if(Validator.isEmpty(data.email)) {
		errors.email = "Email is verplicht";
	} else if(!Validator.isEmail(data.email)) {
		errors.email = "Email is ongeldig";
	}

	//Check wachtwoord
	if(Validator.isEmpty(data.password)) {
		errors.password = "Wachtwoord is verplicht";
	}

	return {
		errors,
		isValid: isEmpty(errors)
	};
};